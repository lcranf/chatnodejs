var net = require('net');
var carrier = require('carrier');

var connections = [];

console.log("Initializing chat server...\n");

net.createServer(function(conn) {
   connections.push(conn);

   conn.write("Hello, welcome to this chat server\n");
   conn.write("Please input your user name:\n");
   
   conn.on('close', function() {
      var index = connnections.index(conn);

      if(index >= 0) {
      	connections.splice(index, 1);
      }
   });

   var username;

   carrier.carry(conn, function(line) {
      if(!username) {
      	username = line;
      	conn.write("Hello " + username + "!\n");
      	return;
      }

      if(line == 'quit') {
      	conn.end();
      	return;
      }

      var output = username + ": " + line + "\n";

      connections.forEach(function(connection) {
      	 if(conn !== connection) {
      	    connection.write(output);	
      	 }         
      });

   });

}).listen(4000);
